#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `ckmseed` package."""

import unittest

from unittest.mock import patch
from ckMseed.ckMseed import main


class TestCkmseed(unittest.TestCase):
    """Tests for `ckmseed` package."""

    def test_import(self):
        """Test ckMseed import"""
        with patch("sys.argv", ["ckmseed", "-#"]):
            with self.assertRaises(SystemExit) as cmd:
                main()
            self.assertEqual(cmd.exception.code, 0, "sys.exit(0) never called "
                             "- Failed to exercise ckMseed")
