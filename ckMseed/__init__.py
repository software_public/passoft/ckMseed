# -*- coding: utf-8 -*-

"""Top-level package for ckMseed."""

__author__ = """EPIC"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2024.3.0.0'
