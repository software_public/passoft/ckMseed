#!/usr/bin/python3

"""
# gui to find and review mseed files
# author: bcb
#
##########################
# modification
# author: bcb
# date 2009.188
#
# bug fix:
# modified to no longer create threads.  Commented out all the lines with
# threading calls so this can later be noted where would be useful to add it
# in again.  The reason for this is because everytime a thread was used it was
# defined, started and then joined immediately.  Therefore, there was no reason
# for them to be threads at all, and it was breaking with Tk because the
# functions that were the targets for the threads weren't actually Tk/thread
# safe.
################################################################
#
# modification
# version: 2020.211
# author: Maeva Pourpoint
#
# Updates to work under Python 3.
# Unit tests to ensure basic functionality.
# Code cleanup to conform to the PEP8 style guide.
# Directory cleanup (remove unused files introduced by Cookiecutter).
# Packaged with conda.
# Remove use of lstrip() in read_all().
################################################################

# modification
# version: 2022.2.0.0
# author: Destiny Kuehn
#
# Change gui to PySide2
################################################################

# modification
# version: 2024.3.0.0
# author: Omid Hosseini
#
# Change gui to PySide6
################################################################
"""

import os
import string
import sys
import time

from getopt import getopt
from glob import glob
from operator import mod
from PySide6.QtWidgets import (QApplication, QWidget, QTabWidget,
                               QGroupBox, QVBoxLayout, QHBoxLayout,
                               QCheckBox, QPushButton, QLineEdit,
                               QLabel, QRadioButton, QComboBox,
                               QFileDialog, QMessageBox,
                               QProgressDialog, QTreeWidget,
                               QTreeWidgetItem)
from PySide6.QtCore import (Qt, QObject, QThread, Signal,
                            QEventLoop, QTimer)
from PySide6.QtGui import QBrush

from ckMseed.LibTrace import *

VERSION = "2024.3.0.0"

# import threading
SPACE = " "

# vars used for batchmode
BATCHMODE = 0


def usage(err=''):
    """
    Usage statements
    """
    print(err)
    print("Usage\nckMseed \nckMseed -#\nckMseed -h\nckMseed [-d DataDirs] "
          "[-s] [-v]")
    print("       -d DataDirs - colon separated list of data directories "
          "[default: cwd]")
    print("       -h Usage")
    print("       -s Simple mode. Check and report endianess of first fixed "
          "header only")
    print("       -v Check and report endianess of first fixed header and "
          "determine")
    print("           start times for first and last block of mseed file")
    print("       -V Read all header blockettes")
    print("       NOTE: -s, -v, & -V are mutually exclusive.-V supercedes -v "
          "supercedes -s")


def main():
    """
    Parse for command line args
    """
    try:
        opts, pargs = getopt(sys.argv[1:], 'hsvV#d:')
    except Exception:
        err = "\nERROR: Invalid command line usage\n"
        usage(err)
        sys.exit(1)
    else:
        dataloc = ""
        runflag = ""
        batchfile = 1
        for flag, arg in opts:
            if flag == "-h":
                usage()
                sys.exit(0)
            if flag == "-d":
                dataloc = arg
            if flag == "-s":
                runflag = "Simple"
            if flag == "-v":
                runflag = "Verbose"
            if flag == "-V":
                runflag = "Very Verbose"
            if flag == "-#":
                print(VERSION)
                sys.exit(0)
    print("\n", os.path.basename(sys.argv[0]), VERSION)
    app = QApplication(sys.argv)
    window = MainWindow(batchfile=batchfile, dataloc=dataloc, runflag=runflag)
    window.show()
    sys.exit(app.exec_())


class MainWindow(QWidget):

    def __init__(self, batchfile, dataloc, runflag):
        super().__init__()

        self.window_setup()
        self.data_init(batchfile, dataloc, runflag)

    def window_setup(self):
        """
        Build main window
        """

        # WINDOW SETTINGS

        self.resize(770, 546)
        self.setWindowTitle("ckMseed " + VERSION)

        # window layout
        self.window_layout = QVBoxLayout()
        self.setLayout(self.window_layout)

        # tab init
        self.tabwidget = QTabWidget()
        self.scan = QWidget()
        self.tabwidget.addTab(self.scan, "Scan")

        # groupbox for popup help and save/exit buttons
        self.mainbox = QGroupBox()
        self.mainbox_layout = QHBoxLayout(self.mainbox)

        # PopUp Help Check box
        self.pop_up_help = QCheckBox("PopUp Help")
        self.pop_up_help.setChecked(False)
        self.pop_up_help.setToolTip("Toggles 'PopUp Help' on and off")
        self.pop_up_help.toggled.connect(self.click_pop_up_help)

        # Save Scan button
        self.save_scan_btn = QPushButton("Save Scan")
        self.save_scan_btn.setStyleSheet("QPushButton::hover"
                                         "{"
                                         "background-color:green;"
                                         "}")
        self.save_scan_btn.clicked.connect(self.clicked_save_scan)

        # Exit button
        self.exit_btn = QPushButton("Exit")
        self.exit_btn.setStyleSheet("QPushButton::hover"
                                    "{"
                                    "background-color:rgb(165, 29, 45);"
                                    "}")
        self.exit_btn.clicked.connect(lambda: sys.exit())

        # add widgets to mainbox
        self.mainbox_layout.addWidget(self.pop_up_help)
        self.mainbox_layout.addStretch(1)
        self.mainbox_layout.addWidget(self.save_scan_btn)
        self.mainbox_layout.addWidget(self.exit_btn)

        # info bar init
        self.infobar = QLineEdit()
        self.infobar.setStyleSheet("background-color:yellow")
        self.infobar.setReadOnly(True)

        # add tab widget to main layout
        self.window_layout.addWidget(self.tabwidget)

        # create and fill other tabs
        self.build_scan()
        self.build_other_tabs()

        # add mainbox and info bar to layout
        self.window_layout.addWidget(self.mainbox)
        self.window_layout.addWidget(self.infobar)
        self.window_layout.setContentsMargins(0, 0, 0, 0)  # set spacing

    def build_scan(self):
        """
        Build widgets in Scan tab
        """

        # init tab layout
        self.scan_layout = QVBoxLayout(self.scan)

        # init groupboxes for data fields
        self.datadir_box = QGroupBox()
        self.datadir_box.setCheckable(False)

        self.stations_box = QGroupBox()
        self.stations_box.setCheckable(False)

        self.radio_box = QGroupBox()
        self.radio_box.setCheckable(False)

        self.scan_box = QGroupBox()
        self.scan_box.setCheckable(False)

        # layouts for groupboxes
        hbox_datadir = QHBoxLayout(self.datadir_box)
        hbox_stations = QHBoxLayout(self.stations_box)
        hbox_rbuttons = QHBoxLayout(self.radio_box)
        vbox_scantext = QVBoxLayout(self.scan_box)

        # data dir widgets
        self.dd_label = QLabel("Data Directories:")
        self.dd_text = QLineEdit()
        self.scan_traces_btn = QPushButton("Scan Traces")
        self.scan_traces_btn.setStyleSheet("""
                                           QPushButton{
                                           background-color:lightblue;
                                           }
                                           QPushButton::hover
                                           {
                                           background-color:green;
                                           }
                                           """)
        self.scan_traces_btn.clicked.connect(self.click_scan_traces)
        self.find_btn = QPushButton("Find")
        self.find_btn.setStyleSheet("QPushButton::hover"
                                    "{"
                                    "background-color:green;"
                                    "}")
        self.find_btn.clicked.connect(self.file_dialogue)
        self.clear_btn = QPushButton("Clear")
        self.clear_btn.setStyleSheet("QPushButton::hover"
                                     "{"
                                     "background-color:yellow;"
                                     "}")
        self.clear_btn.clicked.connect(lambda: self.dd_text.clear())

        # add widgets to layout
        hbox_datadir.addWidget(self.dd_label)
        hbox_datadir.addWidget(self.dd_text)
        hbox_datadir.addWidget(self.scan_traces_btn)
        hbox_datadir.addWidget(self.find_btn)
        hbox_datadir.addWidget(self.clear_btn)

        # stations widgets
        self.stations_label = QLabel("Scan only stations (colon separated list):")
        self.stations_text = QLineEdit()
        self.stations_clear_btn = QPushButton("Clear")
        self.stations_clear_btn.setStyleSheet("QPushButton::hover"
                                              "{"
                                              "background-color:yellow;"
                                              "}")
        self.stations_clear_btn.clicked.connect(lambda: self.stations_text.clear())
        hbox_stations.addWidget(self.stations_label)
        hbox_stations.addWidget(self.stations_text)
        hbox_stations.addWidget(self.stations_clear_btn)

        # rbuttons widgets
        self.scan_type_label = QLabel("Scan Type:")
        self.simple_rbtn = QRadioButton("Simple")
        self.simple_rbtn.setChecked(True)
        self.simple_rbtn.clicked.connect(self.click_scan_type)
        self.verbose_rbtn = QRadioButton("Verbose")
        self.verbose_rbtn.setChecked(False)
        self.verbose_rbtn.clicked.connect(self.click_scan_type)
        self.vv_rbtn = QRadioButton("Very Verbose")
        self.vv_rbtn.setChecked(False)
        self.vv_rbtn.clicked.connect(self.click_scan_type)

        # add widgets to layout
        hbox_rbuttons.addWidget(self.scan_type_label)
        hbox_rbuttons.addWidget(self.simple_rbtn)
        hbox_rbuttons.addWidget(self.verbose_rbtn)
        hbox_rbuttons.addWidget(self.vv_rbtn)
        hbox_rbuttons.addStretch(1)

        # scan text widgets

        # Stat:Chan:Loc:Net:Sps Found label
        self.scan_tree_label = QLabel("Stat:Chan:Loc:Net:Sps Found")
        self.scan_tree_label.setAlignment(Qt.AlignCenter)

        # Stat:Chan:Loc:Net:Sps Found text box
        self.scan_tree = QTreeWidget()
        self.scan_tree.setRootIsDecorated(False)
        self.scan_tree.setColumnCount(5)
        self.scan_tree.setColumnWidth(0, 150)
        self.scan_tree.setColumnWidth(3, 150)

        # tree column labels
        scan_header = QTreeWidgetItem()
        scan_header.setText(0, 'Stat:Chan:Loc:Net:Sps')
        scan_header.setText(1, 'Big')
        scan_header.setText(2, 'Little')
        scan_header.setText(3, 'Start Time')
        scan_header.setText(4, 'End Time')
        for i in range(5):
            scan_header.setBackground(i, Qt.yellow)
        self.scan_tree.setHeaderItem(scan_header)

        # add widgets to layout
        vbox_scantext.addWidget(self.scan_tree_label)
        vbox_scantext.addWidget(self.scan_tree)

        # add groupboxes to tab layout
        self.scan_layout.addWidget(self.datadir_box)
        self.scan_layout.addWidget(self.stations_box)
        self.scan_layout.addWidget(self.radio_box)
        self.scan_layout.addWidget(self.scan_box)

    def build_other_tabs(self):
        """
        Create other tabs (Big Endian, Little Endian, Errors)
        """

        self.build_big_endian()
        self.build_little_endian()
        self.build_errors()

    def build_big_endian(self):
        """
        Build widgets in Big Endian tab
        """

        self.big_endian = QWidget()
        self.tabwidget.addTab(self.big_endian, "Big Endian")
        self.little_endian = QWidget()
        self.tabwidget.addTab(self.little_endian, "Little Endian")
        self.errors = QWidget()
        self.tabwidget.addTab(self.errors, "Errors")

        # layouts
        self.big_endian_layout = QVBoxLayout(self.big_endian)
        self.big_traces_layout = QHBoxLayout()

        # widgets
        self.display_traces_matching = QLabel("Display Traces Matching:")

        self.big_e_dropmenu = QComboBox()
        self.big_e_dropmenu.activated.connect(self.big_e_dropmenu_selections)

        self.big_e_tree = QTreeWidget()
        self.big_e_tree.setColumnCount(5)
        self.big_e_tree.setColumnWidth(1, 160)
        self.big_e_tree.setColumnWidth(2, 150)
        self.big_e_tree.setColumnWidth(3, 150)

        # tree column labels
        big_e_header = QTreeWidgetItem()
        big_e_header.setText(0, '')
        big_e_header.setText(1, 'Trace Name')
        big_e_header.setText(2, 'Start Time')
        big_e_header.setText(3, 'End Time')
        big_e_header.setText(4, 'Blockettes')
        for i in range(5):
            big_e_header.setBackground(i, Qt.yellow)
        self.big_e_tree.setHeaderItem(big_e_header)

        self.big_traces_layout.addWidget(self.display_traces_matching)
        self.big_traces_layout.addWidget(self.big_e_dropmenu)
        self.big_traces_layout.setAlignment(Qt.AlignCenter)

        self.big_endian_layout.addLayout(self.big_traces_layout)
        self.big_endian_layout.addWidget(self.big_e_tree)

    def build_little_endian(self):
        """
        Build widgets in Little tab
        """

        # layouts
        self.little_endian_layout = QVBoxLayout(self.little_endian)
        self.little_traces_layout = QHBoxLayout()

        # 'Display Traces Matching' label
        self.display_traces_matching2 = QLabel("Display Traces Matching:")

        # 'Display Traces Matching' drop menu
        self.little_e_dropmenu = QComboBox()
        self.little_e_dropmenu.activated.connect(self.little_e_dropmenu_selections)

        # Little Endian tree
        self.little_e_tree = QTreeWidget()
        self.little_e_tree.setColumnCount(5)
        self.little_e_tree.setColumnWidth(1, 160)
        self.little_e_tree.setColumnWidth(2, 150)
        self.little_e_tree.setColumnWidth(3, 150)

        # tree column labels
        little_e_header = QTreeWidgetItem()
        little_e_header.setText(0, '')
        little_e_header.setText(1, 'Trace Name')
        little_e_header.setText(2, 'Start Time')
        little_e_header.setText(3, 'End Time')
        little_e_header.setText(4, 'Blockettes')
        for i in range(5):
            little_e_header.setBackground(i, Qt.yellow)
        self.little_e_tree.setHeaderItem(little_e_header)

        self.little_traces_layout.addWidget(self.display_traces_matching2)
        self.little_traces_layout.addWidget(self.little_e_dropmenu)
        self.little_traces_layout.setAlignment(Qt.AlignCenter)

        self.little_endian_layout.addLayout(self.little_traces_layout)
        self.little_endian_layout.addWidget(self.little_e_tree)

    def build_errors(self):
        """
        Build widgets in Errors tab
        """

        self.errors_layout = QVBoxLayout(self.errors)

        self.error_rbtns_box = QGroupBox()
        self.error_rbtns_layout = QHBoxLayout(self.error_rbtns_box)

        # Display label
        self.display_label = QLabel("Display:")
        self.display_label.setToolTip("Filter Error Messages")

        # All button
        self.all_rbtn = QRadioButton("All")
        self.all_rbtn.setChecked(True)
        self.all_rbtn.clicked.connect(self.click_errors_display)
        self.all_rbtn.clicked.connect(self.display_error)

        # Read/Write button
        self.rw_rbtn = QRadioButton("Read/Write")
        self.rw_rbtn.setChecked(False)
        self.rw_rbtn.clicked.connect(self.click_errors_display)
        self.rw_rbtn.clicked.connect(self.display_error)

        # Size button
        self.size_rbtn = QRadioButton("Size")
        self.size_rbtn.setChecked(False)
        self.size_rbtn.clicked.connect(self.click_errors_display)
        self.size_rbtn.clicked.connect(self.display_error)

        # Endian button
        self.endian_rbtn = QRadioButton("Endian")
        self.endian_rbtn.setChecked(False)
        self.endian_rbtn.clicked.connect(self.click_errors_display)
        self.endian_rbtn.clicked.connect(self.display_error)

        # Non-Unique button
        self.non_u_rbtn = QRadioButton("Non-Unique")
        self.non_u_rbtn.setChecked(False)
        self.non_u_rbtn.clicked.connect(self.click_errors_display)
        self.non_u_rbtn.clicked.connect(self.display_error)

        # add widgets to layout
        self.error_rbtns_layout.addWidget(self.display_label)
        self.error_rbtns_layout.addWidget(self.all_rbtn)
        self.error_rbtns_layout.addWidget(self.rw_rbtn)
        self.error_rbtns_layout.addWidget(self.size_rbtn)
        self.error_rbtns_layout.addWidget(self.endian_rbtn)
        self.error_rbtns_layout.addWidget(self.non_u_rbtn)
        self.error_rbtns_layout.addStretch(1)

        # Errors tree
        self.error_tree = QTreeWidget()
        self.error_tree.setHeaderLabel("Errors Found")
        self.error_tree.setRootIsDecorated(False)

        self.errors_layout.addWidget(self.error_rbtns_box)
        self.errors_layout.addWidget(self.error_tree)

    def data_init(self, batchfile, dataloc, runflag):
        """
        Initialize variables and data and check if batchmode
        """

        # set timezone to UTC
        os.environ['TZ'] = 'GMT'
        time.tzname = ('GMT', 'GMT')
        time.timezone = 0

        # Variables
        self.batchfile = batchfile
        self.dataloc = dataloc
        self.data_dirs = ""
        self.stat_sel = ""
        self.save_file = ""
        self.num_big_files = 0
        self.num_little_files = 0
        self.num_errors = 0
        self.scan_type = 0
        self.run_scan = 0
        self.error_type = 0

        # Lists
        self.stat_sel_list = []
        self.data_dir_list = []
        self.stat_sel_list = []
        self.error_dict = []
        self.error_all = []
        self.error_rw = []
        self.error_size = []
        self.error_endian = []
        self.error_unique = []

        # Dictionaries
        self.big_endian_dict = {}
        self.little_endian_dict = {}
        self.error_dict = {}
        self.stat_chan_loc_dict = {}

        if self.dataloc:
            self.dd_text.setText(self.dataloc)
        else:
            self.dd_text.setText(os.getcwd())

        # If batchmode then launch from here and exit
        if BATCHMODE:
            # address endian change first since unique i.e. no batchfile
            print("Running in Batch Mode")
            if RunEndianBatch:
                print("Changing Endianess to: ", Endianess)
                self.to_endian = Endianes
                self.run_change_endian = 1
                print("Finding Files beneath directory: ", self.data_dirs)
                self.build_trace_list()
                self.ChangeEndian()
            else:
                if RunTimeBatch:
                    print("Correcting Timing")
                if RunHeaderBatch:
                    print("Correcting Headers")

                print("Using Template: " + self.batchfile)
                self.LoadTemplate()
                if RunTimeBatch:
                    if not self.UpdateTimeDict:
                        print("No Timing Corrections in Template.\nDone")
                        sys.exit(1)
                    print("Finding Files beneath directory: "
                          + self.data_dirs)
                    self.build_trace_list()
                    self.run_apply_time_cor = 1
                    self.ApplyTimeCor()
                if RunHeaderBatch:
                    if not self.UpdateHdrDict:
                        print("No Header Corrections in Template.\nDone")
                        sys.exit(1)
                    print("Finding Files beneath directory: " +
                          self.data_dirs)
                    self.build_trace_list()
                    self.RunModHdrs = 1
                    self.ModHdrs()
            print("Done")
            sys.exit(0)

    def click_scan_type(self):
        """
        Initialize ScanType
        """
        if self.simple_rbtn.isChecked():
            self.scan_type = 0
        elif self.verbose_rbtn.isChecked():
            self.scan_type = 1
        elif self.vv_rbtn.isChecked():
            self.scan_type = 2

    def click_errors_display(self):
        """
        Initialize ErrorType
        """
        if self.all_rbtn.isChecked():
            self.error_type = 0
        elif self.rw_rbtn.isChecked():
            self.error_type = 1
        elif self.size_rbtn.isChecked():
            self.error_type = 2
        elif self.endian_rbtn.isChecked():
            self.error_type = 3
        elif self.non_u_rbtn.isChecked():
            self.error_type = 4

    def click_pop_up_help(self):
        """
        Initialize tool tips on/off
        """
        if self.pop_up_help.isChecked():
            self.exit_btn.setToolTip("Exit Program")
            self.save_scan_btn.setToolTip("Allows scan results to\n"
                                          "be saved to a file")
            self.clear_btn.setToolTip("Clears 'Data Directories' entry")
            self.find_btn.setToolTip("Dialogue window to\n"
                                     "select Data Directories")
            self.scan_traces_btn.setToolTip("Scan traces using\n"
                                            "'Data Directories' as\n"
                                            "top level directories")
            self.dd_label.setToolTip("Search path(s) for finding mseed files.\n"
                                     "Colon separate multiple entries")
            self.stations_clear_btn.setToolTip("Clear station filter")
            self.scan_type_label.setToolTip("Simple - ID traces\n"
                                            "Verbose - Include Start/End Times\n"
                                            "Very verbose - Scan all blocks")
            self.display_label.setToolTip("Filter Error messages")

        else:
            self.exit_btn.setToolTip("")
            self.save_scan_btn.setToolTip("")
            self.clear_btn.setToolTip("")
            self.find_btn.setToolTip("")
            self.scan_traces_btn.setToolTip("")
            self.dd_label.setToolTip("")
            self.stations_clear_btn.setToolTip("")
            self.scan_type_label.setToolTip("")
            self.display_label.setToolTip("")

    def file_dialogue(self):
        """
        File Dialogue for choosing directories
        """
        self.search = QFileDialog.getExistingDirectory()

        directories = self.dd_text.text()

        # if data directories field is empty, add chosen directory
        if directories == "":
            if self.search != "":  # if user doesn't cancel chosing a directory
                self.dd_text.insert(self.search)
        # if data directories field is not empty, append chosen directory with :
        else:
            if self.search != "":
                self.dd_text.insert(":" + self.search)  # append

    def click_scan_traces(self):
        """
        Check scan type and at least one directory have been selected
        before build_trace_list
        """
        directory = self.dd_text.text()  # gather directory from 'Data Directories'

        # if data directory is empty, an error message will pop up
        if directory == "":
            error_msg = QMessageBox()
            error_msg.setIcon(QMessageBox.Critical)
            error_msg.setText("Error")
            error_msg.setInformativeText("No directories listed.")
            error_msg.setWindowTitle("Error")
            error_msg.exec_()

        else:

            self.reset_data()
            self.build_trace_list()

    def build_trace_list(self):
        """
        Build a trace list using DataDirList as base directories
        """

        directory = self.dd_text.text()

        # begin directory search
        for idir in directory.split(":"):
            dirlist = glob(idir)
            for newdir in dirlist:
                if not os.path.isdir(newdir):
                    err = "***WARNING*** Directory " + newdir + " not found."
                    self.add_text_infobar(err, "red")
                    self.num_errors = self.num_errors + 1
                    return
                self.data_dir_list.append(newdir)

        self.stat_sel = self.stations_text.text()
        if self.stat_sel != "":
            for statsel in self.stat_sel.split(":"):
                self.stat_sel_list.append(statsel)

        self.launch_find_trace()

    def after_find_trace(self):
        """
        Update widgets after find trace
        thread finishes
        """

        numfiles = self.num_big_files + self.num_little_files
        numerrors = self.num_errors
        self.update_infobar(numfiles, numerrors)  # update info

        self.write_scan()
        self.write_big_endian("All")
        self.write_little_endian("All")
        self.display_error()

        QApplication.beep()

    def reset_data(self):
        """
        Delete data from past scans
        """

        # Variables
        self.num_big_files = 0
        self.num_little_files = 0
        self.num_errors = 0

        # Text fields
        self.scan_tree.clear()
        self.big_e_tree.clear()
        self.little_e_tree.clear()
        self.error_tree.clear()
        self.big_e_dropmenu.clear()
        self.little_e_dropmenu.clear()

        # Lists
        self.data_dir_list.clear()
        self.stat_chan_loc_dict.clear()
        self.stat_sel_list.clear()
        self.error_dict.clear()
        self.error_all.clear()
        self.error_rw.clear()
        self.error_size.clear()
        self.error_endian.clear()
        self.error_unique.clear()

        # Dicts
        self.big_endian_dict.clear()
        self.little_endian_dict.clear()
        self.error_dict.clear()
        self.stat_chan_loc_dict.clear()

    def add_to_dict(self, in_dict, _id, in_dir, in_list):
        """
        Function to add data to dictionaries
        """

        try:
            try:
                in_dict[_id][in_dir].append(in_list)
            except KeyError:
                in_dict[_id][in_dir] = []
                in_dict[_id][in_dir].append(in_list)
        except KeyError:
            in_dict[_id] = {}
            in_dict[_id][in_dir] = []
            in_dict[_id][in_dir].append(in_list)

    def wait(self, words, cnt):
        """
        Puts words plus cnt to info bar
        """

        txt = words + str(cnt)
        print(txt)
        # self.scantext.append(txt)

    def write_error(self, info, error_type="", color="black"):
        """
        Writes errors to appropriate list
        """

        if BATCHMODE:
            if info:
                print(info)
        else:
            info = info + "\n"
            self.error_all.append((info, color))

            if error_type == "Read/Write":
                self.error_rw.append((info, color))
            elif error_type == "Size":
                self.error_size.append((info, color))
            elif error_type == "Endian":
                self.error_endian.append((info, color))
            elif error_type == "Unique":
                self.error_unique.append((info, color))
            else:
                pass
        return

    def read_all(self, msfile, numblocks, in_dir, in_file, old_fileid):
        """
        Attempts to read all blockettes and check for consistancy
        """

        # local variables
        lfixedhdr = msfile.fixedhdr
        ltypenxt = msfile.typenxt
        lgetblk = msfile.GetBlk
        lrate = msfile.rate
        lstat = msfile.FH.Stat.strip().decode("utf-8")
        lchan = msfile.FH.Chan.strip().decode("utf-8")
        lloc = msfile.FH.Loc.strip().decode("utf-8")
        lnet = msfile.FH.Net.strip().decode("utf-8")
        fullname = os.path.join(in_dir, in_file)
        warn = ""
        blocks = []
        n = 0
        blksize = msfile.blksize
        while n < numblocks:
            # read & rewrite fixed header
            hdrs = lfixedhdr(n * blksize)
            rate = lrate
            stat = lstat
            chan = lchan
            loc = lloc
            net = lnet
            numblk = hdrs[3][3]
            addseek = hdrs[3][6]
            templist = list(map(str, (stat, chan, loc, net, rate)))
            fileid = ":".join(templist)
            if fileid != old_fileid:
                warn = "WARNING: Multiplexed miniseed file"
                warn1 = "\tFile: " + fullname
            b = 0
            # loop over number of blockettes following fixed header at block n
            while b < numblk:
                seekval = (n * blksize) + addseek
                (blktype, _next) = ltypenxt(seekval)
                blklist = lgetblk(blktype, seekval)
                if not blklist:
                    # if blockette error encountered bail
                    err = "Corrupt mseed: " + fullname
                    self.write_error(err, "Endian", "red")
                    err1 = "\tRecord: " + str(n)
                    self.write_error(err1, "Endian")
                    err2 = "\tUnrecognized Blockette: " + str(blktype)
                    self.add_to_dict(self.error_dict, "Unrecognized Blockette",
                                     in_dir, in_file)
                    self.write_error(err2, "Endian")
                    return None

                if blktype not in blocks:
                    blocks.append(blktype)
                addseek = _next
                b += 1
            old_fileid = fileid
            n += 1
        if warn:
            self.write_error(warn, "Unique", "red")
            self.write_error(warn1, "Unique")
            self.add_to_dict(self.error_dict, "Multiplexed", in_dir, in_file)
            self.num_errors = self.num_errors + 1

        return blocks

    def launch_find_trace(self):
        """
        Separated out so FindTrace could be run as a thread
        """

        # if not BATCHMODE:
        #    self.CancelTL(self.run_scan, "Scan Traces")
        # if not BATCHMODE:
        self.run_scan = 1
        self.begin_thread("Scan Traces",
                          lambda: self.find_trace(self.data_dir_list),
                          self.after_find_trace)
        # else:
        #     self.launchfindTrace()

        # if self.run_scan and not BATCHMODE:
        #    self.KillCancelTL(self.run_scan)

    def find_trace(self, data_dir):
        """
        Scan mseed files and fill lists with data
        """

        # local variables
        lstrftime = time.strftime
        lgmtime = time.gmtime

        stack = []
        for k in range(len(data_dir)):
            if not data_dir[k] in stack:
                stack.append(data_dir[k])

        cnt = 1

        while stack:
            directory = stack.pop()
            if not os.access(directory, 5):
                text = "Skipped: %s" % directory
                text1 = "\t\tAccess Error"
                self.write_error(text, "Read/Write", "red")
                self.write_error(text1, "Read/Write", "black")
                self.num_errors = self.num_errors + 1
                continue

            try:
                listfiles = (dir for dir in os.listdir(directory))
            except Exception as e:
                print("Directory Read Error: %s" % e)
                self.num_errors = self.num_errors + 1
                continue

            while 1:
                try:
                    _file = next(listfiles)
                except StopIteration:
                    break
                # handle cancel request
                if not self.run_scan:
                    break
                # keep user posted of progress
                if mod(cnt, 10):
                    pass
                else:
                    if not BATCHMODE:
                        self.func_worker.update.emit(cnt)
                    else:
                        self.wait("Examining File: ", cnt)
                # build up list of mseed files w/ full pathnames
                fullname = os.path.join(directory, _file)
                if os.path.isfile(fullname):
                    if not os.access(fullname, 6):
                        err = "ERROR: Read/Write permission denied."
                        err1 = "\t File:" + fullname
                        self.write_error(err, "Read/Write", "red")
                        self.write_error(err1, "Read/Write")
                        self.add_to_dict(self.error_dict, "Permission Error",
                                         directory, _file)
                        self.num_errors = self.num_errors + 1
                        continue

                    try:
                        msfile = Mseed(fullname)
                        if msfile.isMseed():
                            try:
                                # simple test to determine if correct size file
                                # numblocks = msfile.numblocks
                                filesize = msfile.filesize
                                blksize = msfile.blksize
                                (numblocks, odd_size) = divmod(
                                    filesize, blksize)
                                if odd_size:
                                    warn = ("WARNING: File size is not an "
                                            "integer number of block size (4096).")
                                    warn += " Start/End times cannot be displayed."
                                    warn += " To fix variable record length, use fixhdr."
                                    warn1 = "\t File:" + fullname
                                    self.write_error(warn, "Size", "red")
                                    self.write_error(warn1, "Size")
                                    self.add_to_dict(
                                        self.error_dict, "Size", directory, _file)
                                    self.num_errors = self.num_errors + 1

                            except Exception:
                                err = ("ERROR: Cannot determine file and "
                                       "block sizes.")
                                err1 = "\t File:" + fullname
                                self.write_error(err, "Size", "red")
                                self.write_error(err1, "Size")
                                self.add_to_dict(
                                    self.error_dict, "Size", directory, _file)
                                self.num_errors = self.num_errors + 1
                                continue

                            # assign header info
                            rate = msfile.rate
                            stat = msfile.FH.Stat.strip().decode("utf-8")
                            chan = msfile.FH.Chan.strip().decode("utf-8")
                            loc = msfile.FH.Loc.strip().decode("utf-8")
                            net = msfile.FH.Net.strip().decode("utf-8")
                            templist = list(map(str, (stat, chan, loc, net,
                                                      rate)))
                            fileid = ":".join(templist)

                            # window on specific stations if exist
                            if self.stat_sel_list:
                                if stat not in self.stat_sel_list:
                                    continue
                            if not self.scan_type:
                                filelist = [_file]
                            else:
                                if odd_size:
                                    start, end, startepoch, endepoch = ('', '', '', '')
                                else:
                                    try:
                                        (startepoch, endepoch) = msfile.FirstLastTime()
                                    except Exception as e:
                                        err = ("ERROR: Cannot determine start and "
                                               "end times.")
                                        err1 = "\t File:" + fullname
                                        self.write_error(err, "Read/Write", "red")
                                        self.write_error(err1, "Read/Write")
                                        self.add_to_dict(
                                            self.error_dict, "Read/Write", directory, _file)
                                        self.num_errors = self.num_errors + 1
                                        continue

                                    endepoch += 1
                                    start = lstrftime('%Y:%j:%H:%M:%S',
                                                      lgmtime(startepoch))
                                    end = lstrftime('%Y:%j:%H:%M:%S',
                                                    lgmtime(endepoch))
                                if self.scan_type == 1:
                                    filelist = [_file, start, end]
                                else:
                                    blklist = self.read_all(msfile,
                                                            numblocks,
                                                            directory,
                                                            _file,
                                                            fileid)
                                    filelist = [_file, start, end] + blklist

                            if not self.scan_type:
                                self.stat_chan_loc_dict[fileid] = []
                            else:
                                if odd_size:
                                    self.stat_chan_loc_dict[fileid] = []
                                    self.stat_chan_loc_dict[fileid].append([startepoch, endepoch])
                                else:
                                    try:
                                        if startepoch < self.stat_chan_loc_dict[fileid][0][0]:
                                            self.stat_chan_loc_dict[fileid][0][0] = startepoch
                                        if endepoch > self.stat_chan_loc_dict[fileid][0][1]:
                                            self.stat_chan_loc_dict[fileid][0][1] = endepoch
                                    except KeyError:
                                        self.stat_chan_loc_dict[fileid] = []
                                        self.stat_chan_loc_dict[fileid].append([startepoch, endepoch])

                            # build endian lists
                            if msfile.byteorder == "big":
                                self.num_big_files = self.num_big_files + 1
                                self.add_to_dict(self.big_endian_dict, fileid,
                                                 directory, filelist)
                            elif msfile.byteorder == "little":
                                self.num_little_files = self.num_little_files + 1
                                self.add_to_dict(self.little_endian_dict, fileid,
                                                 directory, filelist)
                        msfile.close()
                    except Exception as e:
                        err = "ERROR: %s" % e
                        self.write_error(err, "Read/Write", "red")
                        err1 = "\t File:" + fullname
                        self.write_error(err1, "Read/Write")
                        self.add_to_dict(
                            self.error_dict, "Read/Write", directory, _file)
                        self.num_errors = self.num_errors + 1
                cnt += 1

                # add fullname to stack if it is a directory or link
                if os.path.isdir(fullname) or (os.path.islink(fullname) and
                                               not os.path.isfile(fullname)):
                    if fullname not in stack:
                        stack.append(fullname)

        # Add 'All' option to drop menu and set as default choice
        self.big_e_dropmenu.insertItem(0, "All")
        self.big_e_dropmenu.setCurrentIndex(0)
        for indexkey in list(self.big_endian_dict):
            self.big_e_dropmenu.addItem(indexkey)

        self.little_e_dropmenu.insertItem(0, "All")
        self.little_e_dropmenu.setCurrentIndex(0)
        for indexkey in list(self.little_endian_dict):
            self.little_e_dropmenu.addItem(indexkey)

    def write_scan(self):
        """
        Add data found in file scan to Scan tab
        """

        white = Qt.white
        light_blue = Qt.cyan

        if not self.stat_chan_loc_dict:
            return

        keylist = []
        keylist = list(self.stat_chan_loc_dict.keys())
        keylist.sort()

        if keylist:
            c = 0
            for key in keylist:
                scan_info = []
                scan_info.append(key)
                num_big = 0
                num_little = 0

                if key in self.big_endian_dict:
                    for _dir in list(self.big_endian_dict[key].keys()):
                        num_big = num_big + len(self.big_endian_dict[key][_dir])
                if key in self.little_endian_dict:
                    for _dir in list(self.little_endian_dict[key].keys()):
                        num_little = num_little + \
                            len(self.little_endian_dict[key][_dir])
                scan_info.append(num_big)
                scan_info.append(num_little)

                # text = key + "\t" + str(num_big) + "\t" + str(num_little)

                if not self.scan_type:
                    pass
                else:
                    # variable record length
                    start, end = ('', '')
                    try:
                        start = time.strftime('%Y:%j:%H:%M:%S', time.gmtime(
                            self.stat_chan_loc_dict[key][0][0]))
                        end = time.strftime('%Y:%j:%H:%M:%S', time.gmtime(
                            self.stat_chan_loc_dict[key][0][1]))
                    except Exception as e:
                        pass
                    scan_info.append(start)
                    scan_info.append(end)
                    # templist = list(map(str, (start, end)))
                    # text = (text + "\t" +
                    #         "\t".join(templist))  # python 3 compatible

                if c:
                    scan_item = QTreeWidgetItem()
                    k = 0
                    for i in range(len(scan_info)):
                        scan_item.setText(i, str(scan_info[i]))
                        scan_item.setBackground(i, light_blue)
                        k = i
                    # bring line to end of widget
                    while k != 5:
                        k += 1
                        scan_item.setText(k, '')
                        scan_item.setBackground(k, light_blue)
                    self.scan_tree.addTopLevelItem(scan_item)
                    c = 0
                else:
                    scan_item = QTreeWidgetItem()
                    k = 0
                    for i in range(len(scan_info)):
                        scan_item.setText(i, str(scan_info[i]))
                        scan_item.setBackground(i, white)
                        k = i
                    # bring line to end of widget
                    while k != 5:
                        k += 1
                        scan_item.setText(k, '')
                        scan_item.setBackground(k, white)
                    self.scan_tree.addTopLevelItem(scan_item)
                    c += 1
        return

    def write_big_endian(self, key):
        """
        Add data found in file scan to Big Endian tab
        """

        green = Qt.darkGreen
        blue = Qt.blue

        keylist = []
        keylist = list(self.big_endian_dict.keys())
        keylist.sort()

        if not self.big_endian_dict:
            return
        c = 0
        if key == "All":
            for indexkey in list(self.big_endian_dict.keys()):
                index_item = QTreeWidgetItem()
                index_item.setText(0, "<" + indexkey + ">")
                index_item.setForeground(0, QBrush(blue))
                self.big_e_tree.addTopLevelItem(index_item)
                index_item.setFirstColumnSpanned(True)
                for directory in list(self.big_endian_dict[indexkey].keys()):
                    directory_item = QTreeWidgetItem()
                    directory_item.setText(0, "<" + directory + ">")
                    directory_item.setForeground(0, QBrush(green))
                    index_item.addChild(directory_item)
                    directory_item.setFirstColumnSpanned(True)
                    for trace in self.big_endian_dict[indexkey][directory]:
                        trace_item = QTreeWidgetItem()
                        trace_item.setText(1, str(trace[0]))
                        index_item.addChild(trace_item)
                        templist = list(map(str, (trace[1:])))
                        for i in range(len(templist)):
                            trace_item.setText(i + 2, str(templist[i]))
        else:
            numtrace = 0
            index_item = QTreeWidgetItem()
            index_item.setText(0, "<" + key + ">")
            index_item.setForeground(0, QBrush(blue))
            self.big_e_tree.addTopLevelItem(index_item)
            index_item.setFirstColumnSpanned(True)
            for directory in list(self.big_endian_dict[key].keys()):
                directory_item = QTreeWidgetItem()
                directory_item.setText(0, "<" + directory + ">")
                directory_item.setForeground(0, QBrush(green))
                index_item.addChild(directory_item)
                directory_item.setFirstColumnSpanned(True)
                for trace in self.big_endian_dict[key][directory]:
                    trace_item = QTreeWidgetItem()
                    trace_item.setText(1, str(trace[0]))
                    index_item.addChild(trace_item)
                    templist = list(map(str, (trace[1:])))
                    for i in range(len(templist)):
                        trace_item.setText(i + 2, str(templist[i]))
                    numtrace += 1
            index_item.setExpanded(True)
            if numtrace == 1:
                text = "Displaying " + str(numtrace) + " trace."
            else:
                text = "Displaying " + str(numtrace) + " trace."
            self.infobar.setText(text)
            self.infobar.setStyleSheet(
                "background-color:green;")
            self.infobar.setAlignment(Qt.AlignCenter)
        return

    def write_little_endian(self, key):
        """
        Add data found in file scan to Little Endian tab
        """

        green = Qt.darkGreen
        blue = Qt.blue

        keylist = []
        keylist = list(self.little_endian_dict.keys())
        keylist.sort()

        if not self.little_endian_dict:
            return
        c = 0
        if key == "All":
            for indexkey in list(self.little_endian_dict.keys()):
                index_item = QTreeWidgetItem()
                index_item.setText(0, "<" + indexkey + ">")
                index_item.setForeground(0, QBrush(blue))
                self.little_e_tree.addTopLevelItem(index_item)
                index_item.setFirstColumnSpanned(True)
                for directory in list(self.little_endian_dict[indexkey].keys()):
                    directory_item = QTreeWidgetItem()
                    directory_item.setText(0, "<" + directory + ">")
                    directory_item.setForeground(0, QBrush(green))
                    index_item.addChild(directory_item)
                    directory_item.setFirstColumnSpanned(True)
                    for trace in self.little_endian_dict[indexkey][directory]:
                        trace_item = QTreeWidgetItem()
                        trace_item.setText(1, str(trace[0]))
                        index_item.addChild(trace_item)
                        templist = list(map(str, (trace[1:])))
                        for i in range(len(templist)):
                            trace_item.setText(i + 2, str(templist[i]))
        else:
            numtrace = 0
            index_item = QTreeWidgetItem()
            index_item.setText(0, "<" + key + ">")
            index_item.setForeground(0, QBrush(blue))
            self.little_e_tree.addTopLevelItem(index_item)
            index_item.setFirstColumnSpanned(True)
            for directory in list(self.little_endian_dict[key].keys()):
                directory_item = QTreeWidgetItem()
                directory_item.setText(0, "<" + directory + ">")
                directory_item.setForeground(0, QBrush(green))
                index_item.addChild(directory_item)
                directory_item.setFirstColumnSpanned(True)
                for trace in self.little_endian_dict[key][directory]:
                    trace_item = QTreeWidgetItem()
                    trace_item.setText(1, str(trace[0]))
                    index_item.addChild(trace_item)
                    templist = list(map(str, (trace[1:])))
                    for i in range(len(templist)):
                        trace_item.setText(i + 2, str(templist[i]))
                    numtrace += 1
            index_item.setExpanded(True)
            if numtrace == 1:
                text = "Displaying " + str(numtrace) + " trace."
            else:
                text = "Displaying " + str(numtrace) + " trace."
            self.infobar.setText(text)
            self.infobar.setStyleSheet(
                "background-color:green;")
            self.infobar.setAlignment(Qt.AlignCenter)
        return

    def display_error(self):
        """
        Filters Error entries
        """

        self.error_tree.clear()
        if self.error_type == 0:
            textlist = self.error_all
        elif self.error_type == 1:
            textlist = self.error_rw
        elif self.error_type == 2:
            textlist = self.error_size
        elif self.error_type == 3:
            textlist = self.error_endian
        elif self.error_type == 4:
            textlist = self.error_unique
        for (textcmd, color) in textlist:
            textcmd = textcmd.replace('\n', '')
            error_item = QTreeWidgetItem()
            error_item.setText(0, textcmd)
            error_item.setForeground(0, QBrush(color))
            self.error_tree.addTopLevelItem(error_item)

    def update_infobar(self, numfiles, numerrors):
        """
        Updates tab names with number of files/errors found and updates the InfoBar
        """

        # text info bar
        if numerrors == 0:
            self.infobar.setStyleSheet("background-color:green")
        else:
            self.infobar.setStyleSheet("background-color:red")
        text = (str(numfiles) + " unique mseed files found. *** " +
                str(numerrors) + " scan errors.")
        self.infobar.setText(text)
        self.infobar.setAlignment(Qt.AlignCenter)

        # tab info
        self.tabwidget.setTabText(1, "Big Endian (" + str(self.num_big_files) + ")")
        self.tabwidget.setTabText(2, "Little Endian (" + str(self.num_little_files) + ")")
        self.tabwidget.setTabText(3, "Errors (" + str(numerrors) + ")")

    def add_text_infobar(self, str='', bg='yellow'):
        """
        Adds Text and color to InfoBar
        """

        # only bell if error, e.g. color red
        # don't ring bell if clearing text or progress info (e.g. color ==
        # yellow/lightblue)
        if BATCHMODE:
            if str:
                if (string.count(str, 'Examining') or
                        string.count(str, 'Modifying') or
                        string.count(str, 'Correcting') or
                        string.count(str, 'Remaining')):
                    print("\r" + str, end=' ')
                    sys.stdout.flush()
                else:
                    print(str)
        else:
            if bg != "yellow" and bg != "lightblue":
                QApplication.beep()
            text = str
            self.infobar.setText(text)
            self.infobar.setAlignment(Qt.AlignCenter)
            self.infobar.setStyleSheet("background-color:" + bg)

    def big_e_dropmenu_selections(self, event):
        """
        Action function for Big Endian drop menu
        """

        index = self.big_e_dropmenu.currentText()
        self.big_e_tree.clear()
        if index == "All":
            self.write_big_endian("All")
        else:
            self.write_big_endian(index)

    def little_e_dropmenu_selections(self, event):
        """
        Action function for Little Endian drop menu
        """

        index = self.little_e_dropmenu.currentText()
        self.little_e_tree.clear()
        if index == "All":
            self.write_little_endian("All")
        else:
            self.write_little_endian(index)

    def clicked_save_scan(self):
        """
        Action function for clicking Save Scan. Creates default file name
        and calls window pop up function
        """

        (year, month, day, hour, minute, second, weekday,
         yearday, daylight) = time.localtime(time.time())

        templist = list(map(str, (year, yearday, hour, minute)))
        now = ".".join(templist)

        self.save_file = ("ckMseed." + now)

        self.create_save_window(self.save_file)

    def create_save_window(self, save_file):
        """
        Init window to save scan
        """

        # Window settings
        self.save_window = QWidget()
        self.save_window.setFixedSize(400, 200)
        self.save_window.setWindowTitle("Save Scan File as:")

        # Save file name
        self.save_file_name = QLineEdit()
        self.save_file_name.setText(save_file)

        # Initialize buttons
        self.save_btn = QPushButton("Save")
        self.save_btn.setStyleSheet("""
                                    QPushButton{
                                    background-color:rgb(98, 160, 234);;
                                    }
                                    QPushButton::hover
                                    {
                                    background-color:green;
                                    }
                                    """)
        self.save_btn.clicked.connect(lambda: self.write_file())
        self.save_btn.clicked.connect(lambda: self.save_window.close())

        self.cancel_btn = QPushButton("Cancel")
        self.cancel_btn.setStyleSheet("QPushButton::hover"
                                      "{"
                                      "background-color:red;"
                                      "}")
        self.cancel_btn.clicked.connect(lambda: self.close_save_window())

        # Initialize checkboxes
        self.save_all = QCheckBox()
        self.save_all.setText("All")
        self.save_all.toggled.connect(self.save_all_toggled)
        self.save_info = QCheckBox()
        self.save_info.setText("Info")
        self.save_errors = QCheckBox()
        self.save_errors.setText("Error")
        self.save_error_traces = QCheckBox()
        self.save_error_traces.setText("Error Traces")

        # Initialize layouts
        file_layout = QVBoxLayout()
        file_layout.addWidget(self.save_file_name)

        check_box_layout = QHBoxLayout()
        check_box_layout.addWidget(self.save_all)
        check_box_layout.addWidget(self.save_info)
        check_box_layout.addWidget(self.save_errors)
        check_box_layout.addWidget(self.save_error_traces)
        check_box_layout.setAlignment(Qt.AlignCenter)

        buttons_layout = QVBoxLayout()
        buttons_layout.addWidget(self.save_btn)
        buttons_layout.addWidget(self.cancel_btn)
        buttons_layout.setAlignment(Qt.AlignBottom)

        total_layout = QVBoxLayout()
        total_layout.addLayout(file_layout)
        total_layout.addLayout(check_box_layout)
        total_layout.addLayout(buttons_layout)

        self.save_window.setLayout(total_layout)

        self.save_window.show()

    def close_save_window(self):
        """
        Action function to close the save window
        """

        self.save_window.close()
        self.add_text_infobar()

    def save_all_toggled(self):
        """
        If "All" is selected, check all other boxes.
        If "All" is unselected, uncheck all other boxes.
        """

        if self.save_all.isChecked():
            self.save_info.setChecked(True)
            self.save_errors.setChecked(True)
            self.save_error_traces.setChecked(True)
        else:
            self.save_info.setChecked(False)
            self.save_errors.setChecked(False)
            self.save_error_traces.setChecked(False)

    def create_overwrite_window(self):
        """
        If file already exists ask to overwrite
        """

        self.result = ""
        self.warning = QMessageBox()
        self.warning.setIcon(QMessageBox.Warning)
        self.warning.setWindowTitle("Warning")
        self.warning.setText("The file exists!")

        overwrite = self.warning.addButton("Overwrite", QMessageBox.ActionRole)
        cancel = self.warning.addButton("Cancel", QMessageBox.ActionRole)

        self.warning.exec_()

        if self.warning.clickedButton() == overwrite:
            self.result = "overwrite"

        elif self.warning.clickedButton() == cancel:
            self.result = "cancel"
            self.add_text_infobar()

    def write_file(self):
        """
        Write File to disk
        """

        divider = "///////////////////"

        _file = self.save_file_name.text()
        if os.path.isfile(_file):
            self.create_overwrite_window()
        else:
            self.result = "overwrite"

        if self.result == "overwrite":

            if (not self.save_all.isChecked()
                    and not self.save_info.isChecked()
                    and not self.save_errors.isChecked()
                    and not self.save_error_traces.isChecked()):
                oops = "Nothing to save"
                self.add_text_infobar(oops, 'orange')
                return

            try:
                outfile = open(_file, "w")

                if (self.save_all.isChecked()) or (self.save_info.isChecked()):
                    if self.stat_chan_loc_dict:
                        outfile.write("%s\n" % divider)
                        outfile.write("%s\n" % "Trace Information")
                        outfile.write("%s\n" % divider)
                        keylist = []
                        keylist = list(self.stat_chan_loc_dict.keys())
                        keylist.sort()

                        if keylist:
                            for key in keylist:
                                numspace = 24 - len(key)
                                if not self.scan_type:
                                    text = key
                                else:
                                    start = time.strftime('%Y:%j:%H:%M:%S',
                                                          time.gmtime(self.stat_chan_loc_dict[key][0][0]))
                                    end = time.strftime('%Y:%j:%H:%M:%S',
                                                        time.gmtime(self.stat_chan_loc_dict[key][0][1]))
                                    templist = list(map(str, (start, end)))
                                    text = key + numspace * \
                                        SPACE + "   ".join(templist)

                                outfile.write("%s\n" % text)
                    else:
                        outfile.close()
                        os.unlink(_file)
                        oops = "Nothing to save"
                        self.add_text_infobar(oops, 'orange')

                if self.save_errors.isChecked():
                    if not self.save_info.isChecked():
                        outfile.write("%s\n" % divider)
                    else:
                        outfile.write("\n\n%s\n" % divider)
                    outfile.write("%s\n" % "All Errors")
                    outfile.write("%s\n" % divider)
                    if self.error_all:
                        for (text, color) in self.error_all:
                            outfile.write("%s" % text)
                    else:
                        outfile.write("%s\n" % "NONE")

                if self.save_error_traces.isChecked():
                    if (not self.save_info.isChecked() or not
                            self.save_errors.isChecked()):
                        outfile.write("%s\n" % divider)
                    else:
                        outfile.write("\n\n%s\n" % divider)
                    outfile.write("%s\n" % "Traces with Errors")
                    outfile.write("%s\n" % divider)
                    if self.error_dict:
                        for indexkey in list(self.error_dict.keys()):
                            text = "<" + indexkey + ">\n"
                            outfile.write("%s" % text)
                            for directory in list(self.error_dict[indexkey].keys()):  # noqa: E501
                                text2 = "   <" + directory + ">\n"
                                outfile.write("%s" % text2)
                                for trace in self.error_dict[indexkey][directory]:  # noqa: E501
                                    text3 = "\t" + trace + "\n"
                                    outfile.write("%s" % text3)
                    else:
                        outfile.write("%s\n" % "NONE")

                outfile.close()

            except IOError as _e:
                err = "Can't Create %s" % _file, _e
                self.add_text_infobar(err, 'red')
                return

        self.save_window.close()

    def begin_thread(self, title, func, after_func):
        """
        Create thread to run function
        """

        # init thread and worker
        self.thread = QThread(self)
        self.func_worker = Worker(func)

        # set signals/slots
        self.func_worker.update.connect(self.update_progress_window)
        self.thread.started.connect(self.func_worker.run)

        # function to run after thread finishes
        if after_func:
            self.func_worker.finished.connect(after_func)

        # delete worker and thread when done
        self.func_worker.finished.connect(self.thread.quit)
        self.func_worker.finished.connect(self.func_worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)

        # init vars related to progress window
        if not BATCHMODE:
            # get file num for progress window
            numfiles = 0
            text = ""

            if title == "Scan Traces":
                for dir in self.data_dir_list:
                    numfiles += (
                        sum([len(files) for r, d, files in os.walk(dir)]))
                text = title + " is active. Please wait."

            # launch progress window
            self.build_progress_window(title, text, numfiles)
            self.func_worker.finished.connect(
                lambda: self.progress_window.done(0))

            # give progress window a small
            # amount of time to finish
            # loading before
            # starting thread
            loop = QEventLoop(self)
            QTimer.singleShot(100, loop.quit)
            loop.exec_()

        self.thread.start()

    def build_progress_window(self, title, text, max):
        """
        Create progress window to update user
        on status of current process
        """

        self.progress_window = QProgressDialog(
            labelText=text, minimum=0,
            maximum=max, parent=self)

        cancel_b = QPushButton("Cancel")
        cancel_b.setStyleSheet(
            "QPushButton::hover{background-color:red;}")
        cancel_b.clicked.connect(
            lambda: self.stop_thread(title))
        self.progress_window.setCancelButton(cancel_b)
        self.progress_window.open()

    def update_progress_window(self, val):
        """
        Update progress window progress bar
        """

        self.progress_window.setValue(val)

    def stop_thread(self, title):
        """
        Stop the currently running thread
        """

        if title == "Scan Traces":
            self.run_scan = 0


class Worker(QObject):
    """
    Thread worker
    """
    # progress_window progressbar counter
    update = Signal(int)
    # progress_window progressbar max
    new_max = Signal(int)
    finished = Signal()

    def __init__(self, func):
        super().__init__()
        self.func = func

    def run(self):
        self.func()
        self.finished.emit()


if __name__ == "__main__":
    main()
