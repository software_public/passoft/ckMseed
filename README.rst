=======
ckMseed
=======

* Description: Find and review MSEED files

* Usage: ckMseed
         ckMseed -#
         ckMseed -h
         ckMseed [-d DataDirs] [-s] [-v] [-V]

* Free software: GNU General Public License v3 (GPLv3)
