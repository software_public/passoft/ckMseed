=======
History
=======

2009.188 (2018-06-07)
------------------

* First release on new build system.

2018.179 (2018-06-28)
------------------

* 2nd release on new build system.

2020.211 (2020-07-29)
------------------
* Removed use of lstrip() in ckMseed.py
* Fixed issue with exec and variable scope in LibTrace.py
  (see GetBlk() and PutBlk())
* Updated to work with Python 3
* Added a unit test to test ckMseed import
* Updated list of platform specific dependencies to be installed when pip
  installing ckMseed (see setup.py)
* Installed and tested ckMseed against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda packages for ckMseed that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline

2022.2.0.0 (2022-11-17)
-----------------------
* The GUI layout is now using pyside2.
* This version is not back compatible.
* The support for python 3.6, 3.7, and 3.8 has been dropped.


2024.3.0.0 (2024-01-02)
-----------------------
* The GUI layout is now using pyside6.
